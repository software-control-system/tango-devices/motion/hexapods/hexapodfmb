//---------------------------------------------------------------------
//- file : hexapod_definitions.h
//- Author : jean coquet Soleil
//- project : Hexapod Tango Device Server
//- uses YAT toolbox
//---------------------------------------------------------------------


#ifndef __HEXAPOD_DEFINITIONS_H__
#define __HEXAPOD_DEFINITIONS_H__

#include <iostream>
#include <yat/network/ClientSocket.h>

namespace CslHexapodApi
{

//---------------------------------------------------------------------
//-  some define and constants ----------------------------------------
//---------------------------------------------------------------------
const long VIRTUAL_AXIS_NB = 6;
//---------------------------------------------------------------------
typedef enum
{ 
  ROTATION_Z,
  ROTATION_S,
  ROTATION_X, 
  TRANSLATION_X,
  TRANSLATION_S,
  TRANSLATION_Z
}VirtualAxis;

typedef enum 
{
  READ_POS,
  READ_SETPOINT,
  WRITE_POS,
  GET_TCP,
  GET_LCS,
  GET_PMAC_STATUS,
  STOP,
  START,
  ZERO_UC,
  GET_CSSTATUS,
  GET_MOTOR_STATUS,
  SET_TCP,
  SET_LCS,
  HOME,
  KILL,
  ENABLE,
  GET_SPEED,
  SET_SPEED,  
  GET_MOT_POS
}VirtualAxisCmds;

static const std::string cmd_names[19] = 

{
  "readpos",
  "readsetpos",
  "setpos",
  "gettcp\n",
  "getlcs\n",
  "pmacstatus\n",
  "stop\n",
  "start\n",
  "zerouc\n",
  "csstatus 1\n",
  "motstatus",
  "settcp",
  "setlcs",
  "home",
  "killall\n",
  "enableall\n",
  "readspeed\n",
  "setspeed",
  "readmotpos"
};


//- create a dedicated type for configuration
//-------------------------------------------
typedef struct Config
{
  //- ctor -----------------------
  Config () : 
      ip_addr(""), 
      port(0), 
      blocking(true),
      close_on_error(true)
  {
    /* noop ctor */
  }

  //- operator= ------------------
  void operator = (const Config& src)
  {
    ip_addr        = src.ip_addr;
    port           = src.port;
    blocking       = src.blocking;
    close_on_error = src.close_on_error;
  }

  //- members --------------------
  std::string ip_addr;
  size_t port;
  yat::Socket::Protocol protocol;
  bool blocking;
  bool close_on_error;
} Config;


}//- namespace CslHexapodApi


#endif //- __HEXAPOD_DEFINITIONS_H__
