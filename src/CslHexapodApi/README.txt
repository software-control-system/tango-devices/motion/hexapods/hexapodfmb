
Technical requirements
=======================
- The YAT library MUST be used by COSYLAB to manage Exceptions, Sockets , and String/numerical conversions. 
YAT library is available from the public projet tango-ds hosted on Sourceforge :
http://tango-ds.cvs.sourceforge.net/viewvc/tango-ds/Libraries/YAT/
- 

Functional requirements
=======================
The file hexapod.h and hexapod_states_definitions.h defines SOLEIL MANDATORY functional requirements to drive the hexapod and deal with states of the systems


Error handling 
===============
....

Coding requirements
===================
To ease the future maintenance of the library, some good programming practices should be followed by COSYLAB. 
The files hexapod_definitions.h and hexapod.cpp proposes first idea of code implementations.



