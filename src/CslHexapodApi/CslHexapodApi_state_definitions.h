//---------------------------------------------------------------------
//- file : hexapod_support.h
//- Author : jean coquet Soleil
//- project : Hexapod Tango Device Server
//- uses YAT toolbox
//---------------------------------------------------------------------


#ifndef __HEXAPOD_STATE_DEFINITIONS_H__
#define __HEXAPOD_STATE_DEFINITIONS_H__

namespace CslHexapodApi
{
    //- Error related methods
    //- the enum symbols below are just for example. 
    //- Set here the categories of errors useful for the poor human trying to understand 
    //- what has gone wrong
    typedef enum 
    {
      COM_DISCONNECTING,
      COM_DISCONNECTED,
      COM_CONNECTING,
      COM_CONNECTED,     
      COM_FAULT
    } ComState;

    typedef enum 
    {
 	  MOVING,
 	  PROG_RUNNING,
 	  AMPLIFIER_FAULT,
 	  FOLLOWING_ERROR,
 	  NEG_LIMIT_SWITCH,
 	  POS_LIMIT_SWITCH,
 	  OPEN_LOOP,
 	  NOT_HOMED,
 	  STANDBY,
 	  OFF
    } SysState;
 

}//- namespace CslHexapodApi


#endif //- __HEXAPOD_STATE_DEFINITIONS_H__
