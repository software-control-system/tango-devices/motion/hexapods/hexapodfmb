//---------------------------------------------------------------------
//- file : hexapod_support.cpp
//- Author : jean coquet Soleil
//- project : Hexapod Tango Device Server
//- uses YAT toolbox
//---------------------------------------------------------------------

#include "CslHexapodApi.h"
#include <math.h>
#include <yat/utils/XString.h>
#include <yat/time/Timer.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "CslHexapodApi/Observer.h"

//- some define and constants

namespace CslHexapodApi
{
 //- Singleton initialization
  CslHexapod* CslHexapod::_instance = 0;
  //----------------------------------------------------
  //- Ctor ---------------------------------------------
  //----------------------------------------------------
  CslHexapod::CslHexapod ():
     Observable ()
   {

    //- default soft limits .........................
    low_pos_limit[0] = -100; high_pos_limit[0] = 100;
    low_pos_limit[1] = -100; high_pos_limit[1] = 100;
    low_pos_limit[2] = -100; high_pos_limit[2] = 100;
    low_pos_limit[3] = -100; high_pos_limit[3] = 100;
    low_pos_limit[4] = -100; high_pos_limit[4] = 100;
    low_pos_limit[5] = -100; high_pos_limit[5] = 100;

    //- initialize socket --------------
    sock=0;
	com_state = COM_DISCONNECTED;
    moving_timer.set_unit (yat::Timeout::TMO_UNIT_MSEC);
    moving_timer.set_value (50.0);
    moving_timer.enable ();


  }
 
  //----------------------------------------------------
  //- Dtor ---------------------------------------------
  //----------------------------------------------------
  CslHexapod::~CslHexapod (void)
  {
    //- close socket
    close_com ();

  }


  //----------------------------------------------------
  //- Get axis position --------------------------------
  //----------------------------------------------------
  double CslHexapod::get_position (VirtualAxis axis) throw (yat::Exception)
  {

     if (axis<0 || axis>5){
        throw yat::Exception( "SOFTWARE_ERROR",
                              "error on virtual axis number",
                              "CslHexapod::get_position");
    }

    return get_val<double> (READ_POS,axis);

  }

  //----------------------------------------------------
  //- Get axis setpoint -__-----------------------------
  //----------------------------------------------------
  double CslHexapod::get_setpoint (VirtualAxis axis) throw (yat::Exception)
  {

    if (axis<0 || axis>5){
        throw yat::Exception( "SOFTAWARE_ERROR",
                              "error on virtual axis number",
                              "CslHexapod::get_setpoint");
    }

    return get_val<double> (READ_SETPOINT,axis);


  }

  //----------------------------------------------------
  //- Set axis setpoint --------------------------------
  //----------------------------------------------------
  void CslHexapod::set_setpoint (VirtualAxis axis, double value) throw (yat::Exception)
  {

    if ((int) axis<0 || (int) axis>5){
        std::cerr << "CslHexapod::set_setpoint axis number <" << axis << "> out of range [0...5]" << std::endl;
        throw yat::Exception( "SOFTWARE_ERROR",
                              "error on virtual axis number",
                              "CslHexapod::set_setpoint");
    }

    if (value > high_pos_limit[(int) axis]){
        std::cerr << "CslHexapod::set_setpoint axis number <"
	          << axis
		  << "> value ["
		  << value
		  << "] > high limit ["
		  << high_pos_limit[(int) axis]
		  << "]"
		  << std::endl;
        throw yat::Exception( "SOFTWARE_ERROR",
                              "value set to high",
                              "CslHexapod::set_setpoint");
    }

    if (value < low_pos_limit[(int) axis]){
        std::cerr << "CslHexapod::set_setpoint axis number <"
	          << axis
		  << "> value ["
		  << value
		  << "] < low limit ["
		  << low_pos_limit[(int) axis]
		  << "]"
		  << std::endl;
        throw yat::Exception( "SOFTWARE_ERROR",
                              "value set to low",
                              "CslHexapod::set_setpoint");
    }

    moving_timer.restart ();
    set_val (WRITE_POS,axis,value);

  }


  //----------------------------------------------------
  //- Set all 6 axes setpoints -------------------------
  //----------------------------------------------------
  void CslHexapod::set_all_positions (std::vector<double> value) throw (yat::Exception)
  {

	for (int axis=0; axis<6; axis++){

		set_setpoint((VirtualAxis) axis,value.at(axis));
	}
// JIRA TANGODEVICE-774
//  notify various clients to update write parts
	Notify();

  }

  //----------------------------------------------------
  //- Get all 6 axes positions -------------------------
  //----------------------------------------------------
  std::vector<double> CslHexapod::get_axes_positions (void) throw (yat::Exception)
  {

 	std::vector<double> positions;
    positions.reserve(6);


	for (int axis=0; axis<6; axis++) positions.push_back(get_position((VirtualAxis) axis));

    return positions;

  }

  //----------------------------------------------------
  //- Get motor positions ------------------------------
  //----------------------------------------------------
  std::vector<double> CslHexapod::get_motor_positions (void) throw (yat::Exception)
  {

  	std::vector<double> positions;
  	positions.reserve(6);

	for (int i=0;i<6;i++) positions.push_back(get_val<double> (GET_MOT_POS,(VirtualAxis) i));

	return positions;


  }


  //----------------------------------------------------
  //- Get LCS ------------------------------------------
  //----------------------------------------------------
  std::vector<double> CslHexapod::get_lcs () throw (yat::Exception)
  {

    return get_arr (GET_LCS);


  }


  //----------------------------------------------------
  //- Set LCS ------------------------------------------
  //----------------------------------------------------
  void CslHexapod::set_lcs (std::vector<double> dvect) throw (yat::Exception)
  {

    set_arr (SET_LCS,dvect);

  }


  //----------------------------------------------------
  //- Get TCP -----------------------------------------
  //----------------------------------------------------
  std::vector<double> CslHexapod::get_tcp () throw (yat::Exception)
  {
    return get_arr (GET_TCP);
 }


  //----------------------------------------------------
  //- Set TCP ------------------------------------------
  //----------------------------------------------------
  void CslHexapod::set_tcp (std::vector<double> dvect) throw (yat::Exception)
  {
    set_arr (SET_TCP, dvect);
  }

  //----------------------------------------------------
  //- Get speed ----------------------------------------
  //----------------------------------------------------
  double CslHexapod::get_speed () throw (yat::Exception)
  {
    return get_val<double> (GET_SPEED);
  }

  //----------------------------------------------------
  //- Set speed ----------------------------------------
  //----------------------------------------------------
  void CslHexapod::set_speed (double value) throw (yat::Exception)
  {

    set_val (SET_SPEED,value);

  }

  //----------------------------------------------------
  //- Home and return to factory zero ------------------
  //----------------------------------------------------
  void CslHexapod::home () throw (yat::Exception)
  {


    set_val (HOME,0);


  }

  //----------------------------------------------------
  //- Home and return to previous position  ------------
  //----------------------------------------------------
  void CslHexapod::home_and_return_to_original () throw (yat::Exception)
  {


    set_val (HOME,1);


  }

  //----------------------------------------------------
  //- Home and return to zero  -------------------------
  //----------------------------------------------------
  void CslHexapod::home_and_return_to_zero () throw (yat::Exception)
  {


    set_val (HOME,2);


  }

  //----------------------------------------------------
  //- Start movement -----------------------------------
  //----------------------------------------------------
  void CslHexapod::start () throw (yat::Exception)
  {

    send_cmd (START);


  }

  //----------------------------------------------------
  //- Stop movement -----------------------------------
  //----------------------------------------------------
  void CslHexapod::stop () throw (yat::Exception)
  {

    send_cmd (STOP);


  }

  //----------------------------------------------------
  //- Disable amplifiers -------------------------------
  //----------------------------------------------------
  void CslHexapod::disable_amplifiers () throw (yat::Exception)
  {

    send_cmd (KILL);

  }

  //----------------------------------------------------
  //- Enable amplifiers --------------------------------
  //----------------------------------------------------
  void CslHexapod::enable_motors () throw (yat::Exception)
  {


    send_cmd (ENABLE);

  }

  //----------------------------------------------------
  //- Zero UC -----------------------------------
  //----------------------------------------------------
  void CslHexapod::zerouc () throw (yat::Exception)
  {


    send_cmd (ZERO_UC);


  }


  //----------------------------------------------------
  //- CS status ----------------------------------------
  //----------------------------------------------------
  std::vector<long> CslHexapod::get_csstatus (void) throw (yat::Exception)
  {


	std::vector<long> sts;
	sts.reserve(3);

	sts= get_status(GET_CSSTATUS);

	return sts;


  }

  //----------------------------------------------------
  //- Motor status ----------------------------------------
  //----------------------------------------------------
  std::vector<long> CslHexapod::get_motor_status (int motor) throw (yat::Exception)
  {


	std::vector<long> sts;
	sts.reserve(2);

	sts= get_status(GET_MOTOR_STATUS,motor);

	return sts;


  }

  //----------------------------------------------------
  //- Motor status bits --------------------------------
  //----------------------------------------------------
  std::vector<int> CslHexapod::get_motor_status_bits (int motor) throw (yat::Exception)
  {


	std::vector<long> sts;
	sts.reserve(2);

	std::vector<int> bits;
	bits.reserve(9);

	sts= get_status(GET_MOTOR_STATUS,motor);

	bits.push_back((sts.at(1)>>0)&1);            //is in position
	bits.push_back((sts.at(1)>>3)&1);            //is amplifier fault
	bits.push_back((sts.at(1)>>2)&1);            //is fatal following error
	bits.push_back((sts.at(0)>>22)&1);           //is negative limit
	bits.push_back((sts.at(0)>>21)&1);           //is positive limit
	bits.push_back((sts.at(0)>>19)&1);           //is amplifier enabled
	bits.push_back((sts.at(0)>>18)&1);           //is in open loop
	bits.push_back((sts.at(1)>>10)&1);           //is homed
	bits.push_back((sts.at(0)>>17)&1);           //is motion program running

	return bits;


  }

  //----------------------------------------------------
  //- Is motor moving--- -------------------------------
  //----------------------------------------------------
  int CslHexapod::isMoving (int motor) throw (yat::Exception)
  {

  	    SysState state;
  	    state=get_motor_state(motor);

     	if (state == MOVING) return 1;
     	else return 0;


  }

  //----------------------------------------------------
  //- Is motor amplifier fault -------------------------------
  //----------------------------------------------------
  int CslHexapod::isAmplifierFault (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

		return sts.at(1);
  }

  //----------------------------------------------------
  //- Is motor fatal following error -------------------------
  //----------------------------------------------------
  int CslHexapod::isFollowingError (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	return sts.at(2);

  }


  //----------------------------------------------------
  //- Is negative limit on -------------------------------
  //----------------------------------------------------
  int CslHexapod::isNegativeOn (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	return sts.at(3);

  }

  //----------------------------------------------------
  //- Is positive limit on -------------------------------
  //----------------------------------------------------
  int CslHexapod::isPositiveOn (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	return sts.at(4);

  }

  //----------------------------------------------------
  //- Is amplifier enabled ----------------------------------
  //----------------------------------------------------
  int CslHexapod::isAmplifierEnabled (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	return sts.at(5);

  }

  //----------------------------------------------------
  //- Is motor activated (in closed loop) --------------
  //----------------------------------------------------
  int CslHexapod::isMotorActivated (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

     	if (sts.at(6)==0) return 1;
    		else return 0;

  }



  //----------------------------------------------------
  //- Is motor homed ---------- -------------------------------
  //----------------------------------------------------
  int CslHexapod::isHomed (int motor) throw (yat::Exception)
  {
     	std::vector<int> sts;
     	sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	return sts.at(7);

  }


  //----------------------------------------------------
  //- Motor state ------ -------------------------------
  //----------------------------------------------------
  SysState CslHexapod::get_motor_state (int motor){

  		std::vector<int> sts;
  		sts.reserve(9);

     	sts= get_motor_status_bits(motor);

    	if (sts.at(0)==0 && sts.at(6)==0) return MOVING;
		if (sts.at(8)) return PROG_RUNNING;
		if (sts.at(1)) return AMPLIFIER_FAULT;
		if (sts.at(2)) return FOLLOWING_ERROR;
		if (sts.at(3)) return NEG_LIMIT_SWITCH;
		if (sts.at(4)) return POS_LIMIT_SWITCH;
		if (sts.at(6)) return OPEN_LOOP;
		if (!sts.at(7)) return NOT_HOMED;
		if (sts.at(5)) return STANDBY;
		return OFF;
  }

  //----------------------------------------------------
  //- CS status bits --------------------------------
  //----------------------------------------------------
  std::vector<int> CslHexapod::get_cs_status_bits () throw (yat::Exception)
  {


		std::vector<long> sts;
		sts.reserve(2);

		std::vector<int> bits;
		bits.reserve(3);

		sts= get_status(GET_CSSTATUS);

		bits.push_back((sts.at(1)>>17)&1);            //is moving
		bits.push_back((sts.at(1)>>20)&1);            //is amplifier fault
		bits.push_back((sts.at(1)>>19)&1);            //is fatal following error

		return bits;


  }

  //----------------------------------------------------
  //- Is hexapod moving--- -------------------------------
  //----------------------------------------------------
  int CslHexapod::isMoving () throw (yat::Exception)
  {

    	SysState state;

     	for (int i=0;i<6;i++){
     		state=get_motor_state(i);
     		if (state != MOVING) return 0;
     	}

    	return 1;

  }

  //----------------------------------------------------
  //- Is amplifier fault -------------------------------
  //----------------------------------------------------
  int CslHexapod::isAmplifierFault () throw (yat::Exception)
  {
      	std::vector<int> sts;
      	sts.reserve(3);

     	sts= get_cs_status_bits();

    	return sts.at(1);

  }

  //----------------------------------------------------
  //- Is fatal following error -------------------------
  //----------------------------------------------------
  int CslHexapod::isFollowingError () throw (yat::Exception)
  {
      	std::vector<int> sts;
      	sts.reserve(3);

     	sts= get_cs_status_bits();

    	return sts.at(2);

  }



  //----------------------------------------------------
  //- Hexapod state ------ -------------------------------
  //----------------------------------------------------
  SysState CslHexapod::get_hexapod_state (){

  		std::vector<int> cssts;
  		cssts.reserve(3);

     	cssts= get_cs_status_bits();

     	std::vector<int> mot_sts[6];

     	for (int i=0;i<6;i++){
     		mot_sts[i]=get_motor_status_bits(i);
     	}

      if (!moving_timer.expired ()) return MOVING;

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(0)==0 && mot_sts[i].at(6)==0) return MOVING;
     	}

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(8)) return PROG_RUNNING;
     	}

     	if(cssts.at(1)) return AMPLIFIER_FAULT;
     	if(cssts.at(2)) return FOLLOWING_ERROR;

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(3)) return NEG_LIMIT_SWITCH;
     	}

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(4)) return POS_LIMIT_SWITCH;
     	}

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(6)) return OPEN_LOOP;
     	}

     	for (int i=0;i<6;i++){
     		if (!mot_sts[i].at(7)) return NOT_HOMED;
     	}

     	for (int i=0;i<6;i++){
     		if (mot_sts[i].at(5)) return STANDBY;
     	}

 		return OFF;
  }

  //----------------------------------------------------
  //- Communication state ------ -------------------------------
  //----------------------------------------------------
  ComState CslHexapod::get_com_state ()
  {
  	return com_state;
  }

  //-----------------------------------------------------
  //- returns a string with the informations on health and errors
  //----------------------------------------------------
  std::string CslHexapod::get_status_as_string (SysState state)
  {
		if (com_state == COM_FAULT)
      return std::string ("COM_FAULT: communication to HEMCI is not working properly.");
		else if (com_state != COM_CONNECTED)
      return std::string ("COM_FAULT: not connected to HEMCI.");

    switch (int (state))
    {
    case MOVING:
      return "Hexapod is moving";
      break;
    case PROG_RUNNING:
      return std::string ("Motion program is running.");
      break;
    case AMPLIFIER_FAULT:
      return std::string ("At least one of the motors has experienced amplifier fault.\n\
  						Check driver for errors.");
      break;
    case FOLLOWING_ERROR:
      return std::string ("At least one of the motors has experienced fatal following error\n. \
  							Check motor encoders and device for obstacles.");
      break;
    case NEG_LIMIT_SWITCH:
    case POS_LIMIT_SWITCH:
      return std::string ("Hardware limits were hit.");
      break;
    case OPEN_LOOP:
      return std::string ("At least one of the motors is in open loop.\n\
  						             Put all motors in closed loop using the enable_motors() command.");
      break;
    case NOT_HOMED:
      return std::string ("At least one of the motors is not homed and hexapod movements can not be performed.\n\
  									       Home the hexapod using one of the home commands.");
      break;

    case STANDBY:
      return std::string ("Hexapod is ready for next move.");
    case OFF:
      return std::string ("At least one of the motors has amplifier disabled.\n\
  									   Re-enable all motors using the enable_motors() command.");
      break;
    default:
      return std::string ("could not populate sys state, uknown or not listed SysState encountered");
      break;

    }




  }

  //----------------------------------------------------
  //- Get value  -------------------------------------
  //----------------------------------------------------
  template< typename T > T CslHexapod::get_val ( VirtualAxisCmds cmd, VirtualAxis axis) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::get_val: VirtualAxisCmds cmd, VirtualAxis axis" <<std::endl;
    std::string reply;
    std::stringstream tmp;
    tmp << cmd_names[cmd].c_str() << " " << (int )(axis+1) << std::ends;
    std::string msg (tmp.str ());
    hemciComm(msg, reply);

    T val= yat::XString<T>::to_num(reply);
    return val;
  }

  //----------------------------------------------------
  //- Get value  -------------------------------------
  //----------------------------------------------------
  template< typename T > T CslHexapod::get_val ( VirtualAxisCmds cmd) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::get_val VirtualAxisCmds cmd" <<std::endl;
    std::string reply;
    std::string msg (cmd_names [cmd]);
    hemciComm(msg, reply);

    T val= yat::XString<T>::to_num(reply);
    return val;
  }

  //----------------------------------------------------
  //- Get array                  -----------------------
  //----------------------------------------------------
  std::vector<double> CslHexapod::get_arr ( VirtualAxisCmds cmd) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::get_arr" <<std::endl;
    std::string reply;

    std::vector<double> dvect;
    std::string msg (cmd_names[cmd]);
    dvect.reserve(6);
    hemciComm(msg, reply);

    std::istringstream is (reply);
    double tmp;

    while (is)
    {
      is >> tmp;
      dvect.push_back (tmp);
    }
    return dvect;
  }


  //----------------------------------------------------
  //- Get status ---------------------------------------
  //----------------------------------------------------
  std::vector<long> CslHexapod::get_status( VirtualAxisCmds cmd) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::get_status cmd number << " << cmd <<std::endl;
    std::string reply;
    std::string msg (cmd_names[cmd]);

    std::vector<long> lvect;
    lvect.reserve(6);

    hemciComm(msg, reply);

    std::istringstream is (reply);
    //std::cout << "CslHexapod::get_status reply = " << reply << std::endl;
    long tmp;
    while (is)
    {
     //std::cout << "CslHexapod::get_status is  " << is <<std::endl;
     is >> std::hex >> tmp;
      lvect.push_back (tmp);
    }
    return lvect;
  }

  //----------------------------------------------------
  //- Get status (id) -----------------------
  //----------------------------------------------------
  std::vector<long> CslHexapod::get_status ( VirtualAxisCmds cmd, int indx) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::get_status: VirtualAxisCmds cmd, int indx" <<std::endl;
    std::string reply;
    std::stringstream tmp;

    tmp << cmd_names[cmd].c_str() << " " << (indx + 1) << std::ends;
    std::string msg = tmp.str ();

    std::vector<long> lvect;
    lvect.reserve(6);

    hemciComm(msg, reply);
    std::istringstream is (reply);

    long l;
    while (is)
    {
      is >> std::hex >> l;
      lvect.push_back (l);
    }
    return lvect;
  }

  //----------------------------------------------------
  //- Set value int ------------------------------------
  //----------------------------------------------------
  void CslHexapod::set_val ( VirtualAxisCmds cmd, int value) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::set_val int" <<std::endl;
    std::string reply;
    std::stringstream tmp;

    tmp << cmd_names[cmd].c_str() << " " << value << std::ends;
    std::string msg = tmp.str ();

    hemciComm(msg, reply);
    if (reply.find ("OK") == std::string::npos)
    {
      throw yat::Exception( "RESPONSE_ERROR",
                            "Reply did not contain OK.",
                            "CslHexapod::set_val");
    }
  }

  //----------------------------------------------------
  //- Set value double----------------------------------
  //----------------------------------------------------
  void CslHexapod::set_val ( VirtualAxisCmds cmd, double value) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::set_val double" <<std::endl;
    std::string reply;
    std::stringstream tmp;
    tmp << cmd_names[cmd].c_str() << " " << value << std::ends;
    std::string msg (tmp.str ());
    hemciComm(msg, reply);
    if (reply.find ("OK") == std::string::npos)
    {
      throw yat::Exception( "RESPONSE_ERROR",
                            "Reply did not contain OK.",
                            "CslHexapod::set_val");
    }
  }
  //----------------------------------------------------
  //- Set value double ---------------------------------
  //----------------------------------------------------
  void CslHexapod::set_val ( VirtualAxisCmds cmd, VirtualAxis axis , double value) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::set_val axis double" <<std::endl;
    std::string reply;
    std::stringstream tmp;
    tmp << cmd_names[cmd].c_str() << " " << int (axis+1) << " " << value << std::ends;
    std::string msg (tmp.str ());
    hemciComm(msg, reply);
    if (reply.find ("OK") == std::string::npos)
    {
      throw yat::Exception( "RESPONSE_ERROR",
                            "Reply did not contain OK.",
                            "CslHexapod::set_val");
    }
  }

  //----------------------------------------------------
  //- Set array -------------------------------------
  //----------------------------------------------------
  void CslHexapod::set_arr ( VirtualAxisCmds cmd , std::vector<double> dvect) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::set_arr" <<std::endl;
    std::string reply;
    std::stringstream tmp;
    tmp << cmd_names[cmd].c_str();
    for (size_t i=0; i< dvect.size(); i++)
    {
      tmp << cmd_names[cmd].c_str() << " " << dvect.at(i);
    }
    tmp << cmd_names[cmd].c_str();
    std::string msg (tmp.str ());
    hemciComm(msg, reply);
    if (reply.find ("OK") == std::string::npos)
    {
      throw yat::Exception( "RESPONSE_ERROR",
                            "Reply did not contain OK.",
                            "CslHexapod::set_arr");
    }
  }

 //----------------------------------------------------
  //- Send command -------------------------------------
  //----------------------------------------------------
  void CslHexapod::send_cmd ( VirtualAxisCmds cmd ) throw (yat::Exception)
  {
    //std::cout << "CslHexapod::send cmd cmd number = " << cmd  <<std::endl;
    //std::cout << "CslHexapod::send cmd cmd = " << cmd_names[cmd]  <<std::endl;
    std::string reply;
    std::string msg (cmd_names[cmd]);
    hemciComm(msg, reply);
    if (reply.find ("OK") == std::string::npos)
    {
      throw yat::Exception( "RESPONSE_ERROR",
                            "Reply did not contain OK.",
                            "CslHexapod::send_cmd");
    }
  }

  //----------------------------------------------------
  //- open the socket
  //----------------------------------------------------
  void CslHexapod::open_com (const Config & conf) throw (yat::Exception)
  {

    //- socket (re)construction
    if(com_state == COM_CONNECTED)
    {
      //- should destroy/rebuild the socket
      this->close_com ();
    }

    //- build the socket
    this->com_state = COM_CONNECTING;

    try
    {
      //- yat internal cooking
      yat::Socket::init();

      //- instanciating the Socket (default protocol is yat::Socket::TCP_PROTOCOL)
      //std::cout << "Instanciating the Socket" << std::endl;
      sock = new yat::ClientSocket ();

      if (conf.blocking){sock->set_blocking_mode();}
          else {sock->set_non_blocking_mode();}

      //- set some socket option
      //std::cout << "Setting sock options" << std::endl;
      sock->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
      sock->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
      sock->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, 1000);
      sock->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, 1000);


      //- network address
      //std::cout << "Instanciating network address" << std::endl;
      yat::Address addr(conf.ip_addr, conf.port);

      //- connect to addr
      //std::cout << "Connecting to peer..." << std::endl;
      sock->connect(addr);

    }
    catch (const yat::SocketException & se)
    {
      //std::cout << "*** yat::SocketException caught ***" << std::endl;
      for (size_t err = 0; err  < se.errors.size(); err++)
      {
        //std::cout << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
        //std::cout << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
        //std::cout << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
        //std::cout << "Err-" << err << "::code....." << se.errors[err].code << std::endl;
      }
      this->com_state = COM_DISCONNECTED;

      throw yat::Exception( se.errors[0].reason,
                        se.errors[0].origin,
                        "CslHexapod::open_com");
    }

    this->com_state = COM_CONNECTED;

  }

  //----------------------------------------------------
  //- close the socket
  //----------------------------------------------------
  void CslHexapod::close_com (void) throw (yat::Exception)
  {
    com_state = COM_DISCONNECTING;
    sock->disconnect();

    yat::Socket::terminate();
    com_state = COM_DISCONNECTED;
  }


  //----------------------------------------------------
  //- sends/receive commands to HW (string version)
  //----------------------------------------------------
  void CslHexapod::hemciComm (std::string & msg, std::string & reply) throw (yat::Exception)
  {

//-    std::cout << "CslHexapod::hemciComm msg= " <<  msg << std::endl;
   int nread = 0;
   if ((com_state != COM_CONNECTED) || (sock==0))
   {
     throw yat::Exception( "COMMUNICATION_ERROR",
                            "Socket not connected",
                            "CslHexapod::hemciComm");
   }

   try
   {
     //- CRITICAL SECTION
     yat::AutoMutex <yat::Mutex> gard (this->m_lock);
     //std::cout<< "send-> "<< msg << std::endl;
     sock->send (msg);
     nread = sock->receive (reply);
 //- std::cout << "CslHexapod::hemciComm reply= " <<  reply << std::endl;
    }
   catch (const yat::SocketException & se)
   {
     this->com_state = COM_FAULT;
     this->com_error_str= "SocketException caugth trying to send command [";
     throw;
   }
   catch (...)
   {
     this->com_state = COM_FAULT;
     this->com_error_str= "(...) caugth trying to send command [";
     throw yat::Exception( "UNKNOWN_ERROR",
                           "caught (...) trying to read/write on socket",
                           "CslHexapod::hemciComm");
   }
   if (nread == 0)
   {
     std::stringstream s;
     s << "response to cmd [" << msg << "] is empty! ";
     throw yat::Exception( "DATA_OUT_OF_RANGE",
                           s.str ().c_str (),
                           "CslHexapod::hemciComm");
   }
 }

}//- namespace CslHexapodApi

