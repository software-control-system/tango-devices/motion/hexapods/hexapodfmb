//---------------------------------------------------------------------
//- file : hexapod_support.h
//- Author : jean coquet Soleil
//- project : Hexapod Tango Device Server
//- uses YAT toolbox
//---------------------------------------------------------------------


#ifndef __HEXAPOD_SUPPORT_H__
#define __HEXAPOD_SUPPORT_H__
#include "CslHexapodApi_definitions.h"
#include "CslHexapodApi_state_definitions.h"
#include "Observer.h"
#include <vector>
#include <yat/threading/Mutex.h>
#include <yat/time/Timer.h>

namespace CslHexapodApi 
{
const int MAX_STRING_LENGTH=512;

//- the HEXAPOD CLASS ------------
  
     class CslHexapod: public Observable
	{
  
 public :

	//-------------------------------------------
	//- Singleton : retourne un pointeur sur l'instance
	//-------------------------------------------
static CslHexapod * instance (void)
		{
			if (_instance == 0)
				_instance = new CslHexapod ();
			return _instance;
		}
    //-opens the communication
    void open_com (const Config &) throw (yat::Exception);
    //-closes the communication
    void close_com (void) throw (yat::Exception);
    //-communication with HEMCI
    void hemciComm (std::string & msg, std::string & reply) 
      throw (yat::Exception);

    //- Manage position in the Laboratory Coordinate System
    //- get real position of virtual axis
    double get_position (VirtualAxis axis) throw (yat::Exception);
     //- get setpoint position of virtual axis
    double get_setpoint (VirtualAxis axis) throw (yat::Exception);
    //- set new position for the virtual axis
    void   set_setpoint (VirtualAxis axis, double position) throw (yat::Exception);
    //- set the 6 positions for the 6 virtual Axes
    void   set_all_positions (std::vector<double> positions) throw (yat::Exception);
    //- get the virtual axes positions 
    std::vector<double> get_axes_positions (void) throw (yat::Exception);
    //- get the motor positions for the 6 legs
    std::vector<double> get_motor_positions (void) throw (yat::Exception);

    //- accessors/mutators for lcs offsets
    // Get the 6 offsets in the LCS coordinate system
    std::vector<double> get_lcs (void) throw (yat::Exception);
    // Set the 6 offsets in the LCS coordinate system
    void set_lcs (std::vector<double> offsets) throw (yat::Exception);
    // Get the 6 offsets in the TCP coordinate system
    std::vector<double> get_tcp (void) throw (yat::Exception);
    // Set the 6 offsets in the TCP coordinate system
    void set_tcp (std::vector<double> offsets) throw (yat::Exception);

    //- accessor/mutator for speed (speed is a % of full speed?)
    double get_speed (void
    ) throw (yat::Exception);
    void set_speed (double) throw (yat::Exception);


    //- movement commands
    //- start moving
    void start (void) throw (yat::Exception);    
    //- normal stop
    void stop (void)  throw (yat::Exception);
	//- emergency stop (could have to redo homing)
    void disable_amplifiers (void) throw (yat::Exception);
    //- enable amplifiers and put motos in closed loop
    void enable_motors (void) throw (yat::Exception);
    
    //- homing
    //- stay at home position
    void home (void) throw(yat::Exception);
    //- return to original position
    void home_and_return_to_original (void) throw (yat::Exception);
    //- Find reference and go to zero (in LCS).
    void home_and_return_to_zero (void) throw (yat::Exception);

    //- miscelleanous methods
    //- soft limits
    void set_soft_limits (VirtualAxis va, double low_lim, double high_lim){
    	low_pos_limit[(int) va]=low_lim;
    	high_pos_limit[(int) va]=high_lim;
    }

	void get_soft_limits (VirtualAxis va, double* low_lim, double* high_lim){
    	*low_lim=low_pos_limit[(int) va];
    	*high_lim=high_pos_limit[(int) va];
    }
    //- "zerouc" command 
    void zerouc (void) throw (yat::Exception);

    //------------------STATES AND STATUS-----------------------------------
    //- returns 3 words for CS status
    std::vector<long> get_csstatus (void) throw (yat::Exception);
    //- returns 2 words for motor status
    std::vector<long> get_motor_status (int motor) throw (yat::Exception);
    //- returns the 6 motor states
    std::vector<SysState> get_motor_states (void);
    //- returns the Motor State
    SysState get_motor_state (int motor_number);
    //- returns the Hexapod State, should report if necessary HEMCI and PMAC errors too
    SysState get_hexapod_state (void);
    //- return the communication state
    ComState get_com_state (void);
    //- returns a string with the informations on health and errors 
    // this will be mapped to the Tango status so text should be human understandable
    std::string get_status_as_string (SysState hexapod_state);

    //------------------ DETAILED STATES (for expert use if needed by SOLEIL) -----------------------------------
 	//- returns status bits for a motor
    std::vector<int> get_motor_status_bits (int motor) throw (yat::Exception);
    //- returns 1 if motor is activated
    int isMotorActivated (int motor) throw (yat::Exception);
    //- returns 1 if motor is enabled
    int isAmplifierEnabled (int motor) throw (yat::Exception);
    //- returns 1 if negative limit is triggered
	int isNegativeOn (int motor) throw (yat::Exception);
	//- returns 1 if positive limit is triggered
	int isPositiveOn (int motor) throw (yat::Exception);
	//- returns 1 if the motor is homed
	int isHomed (int motor) throw (yat::Exception);
	//- returns 1 if the motor is moving
	int isMoving (int motor) throw (yat::Exception);
	//- returns 1 if the motor has amplifier fault
	int isAmplifierFault (int motor) throw (yat::Exception);
	//- returns 1 if any the motor has experienced fatal following error
	int isFollowingError (int motor) throw (yat::Exception);
	//- returns status bits for CS
        std::vector<int> get_cs_status_bits () throw (yat::Exception);
	//- returns 1 if any of the motors have amplifier fault
	int isAmplifierFault () throw (yat::Exception);
	//- returns 1 if any of the motors have experienced fatal following error
	int isFollowingError () throw (yat::Exception);
	//- returns 1 if any of the motors are moving
	int isMoving () throw (yat::Exception);
	
  private:
  
  //- Ctor  -----------------
 	CslHexapod ();
    //- Dtor ---------------------
    virtual ~CslHexapod ();
	   	//- pointer on singleton
    static CslHexapod* _instance;
	//- the client socket class pointer
    yat::ClientSocket * sock;   
    ComState com_state;
    std::string com_error_str;
    
    //- software limits shoud be shared between the instances the class in a single exe
    double low_pos_limit[6]; 
    double high_pos_limit[6];
    
    //- moving state stuff
    yat::Timeout moving_timer;

    //------------------ End of MANDATORY requirements
     // Following methods are given as an example of how SOLEIL we would like the code to be written
    //- Communication with HEMCI
    //- get value
    template< typename T > T get_val (VirtualAxisCmds cmd, VirtualAxis axis) throw (yat::Exception);
    template< typename T > T get_val (VirtualAxisCmds cmd) throw (yat::Exception);
    
    //- get an array
    std::vector<double> get_arr (VirtualAxisCmds cmd) throw (yat::Exception);   
    std::vector<long> get_status (VirtualAxisCmds cmd) throw (yat::Exception);
    std::vector<long> get_status (VirtualAxisCmds cmd, int indx) throw (yat::Exception);
        
    //- write value
    void set_val (VirtualAxisCmds cmd, int value) throw (yat::Exception);   
    void set_val (VirtualAxisCmds cmd, double value) throw (yat::Exception);   
    void set_val (VirtualAxisCmds cmd, VirtualAxis axis, int value) throw (yat::Exception);
    void set_val (VirtualAxisCmds cmd, VirtualAxis axis, double value) throw (yat::Exception);
    
    //- write an array of double
    void set_arr (VirtualAxisCmds cmd, std::vector<double> value) throw (yat::Exception);
    
    //- send command
    void send_cmd (VirtualAxisCmds cmd) throw (yat::Exception);
    
    //- mutex to protect hemciComm against reentrance 
     yat::Mutex m_lock;

	
  };
 


}//- namespace CslHexapod


#endif //- __HEXAPOD_SUPPORT_H__
