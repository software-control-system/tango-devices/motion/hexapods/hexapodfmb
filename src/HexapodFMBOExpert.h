//=============================================================================
//
// file :        HexapodFMBOExpert.h
//
// description : Include for the HexapodFMBOExpert class.
//
// project :	HexapodFMBOExpert
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
//
// $Log: HexapodFMBOExpert.h,v $
// Revision 1.1  2010/06/28 10:40:36  olivierroux
// - initial import
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _HexapodFMBOExpert_H
#define _HexapodFMBOExpert_H

#include <tango.h>
#include "CslHexapodApi/CslHexapodApi.h"

//using namespace Tango;

/**
 * @author	$Author: olivierroux $
 * @version	$Revision: 1.1 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------

namespace HexapodFMBOExpert_ns
{

/**
 * Class Description:
 * Tango support for hexapod using hemci interface.
 */

/*
 *	Device States Description:
*  Tango::STANDBY :  Hexapod ready for next move.
*  Tango::MOVING :   Hexapod is moving.
*  Tango::RUNNING :  Motion program is running, but motors are not moving.
*  Tango::FAULT :    Hexapod is not working properly.
 */


class HexapodFMBOExpert: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevDouble	*attr_RsPosition_read;
		Tango::DevDouble	*attr_RxPosition_read;
		Tango::DevDouble	*attr_RzPosition_read;
		Tango::DevDouble	*attr_TsPosition_read;
		Tango::DevDouble	*attr_TxPosition_read;
		Tango::DevDouble	*attr_TzPosition_read;
		Tango::DevDouble	*attr_RsSetPosition_read;
		Tango::DevDouble	attr_RsSetPosition_write;
		Tango::DevDouble	*attr_RxSetPosition_read;
		Tango::DevDouble	attr_RxSetPosition_write;
		Tango::DevDouble	*attr_RzSetPosition_read;
		Tango::DevDouble	attr_RzSetPosition_write;
		Tango::DevDouble	*attr_TsSetPosition_read;
		Tango::DevDouble	attr_TsSetPosition_write;
		Tango::DevDouble	*attr_TxSetPosition_read;
		Tango::DevDouble	attr_TxSetPosition_write;
		Tango::DevDouble	*attr_TzSetPosition_read;
		Tango::DevDouble	attr_TzSetPosition_write;
		Tango::DevBoolean	*attr_Moving_read;
		Tango::DevBoolean	*attr_Fault_read;
		Tango::DevBoolean	*attr_AllHomed_read;
		Tango::DevDouble	*attr_Speed_read;
		Tango::DevDouble	attr_Speed_write;
		Tango::DevLong	*attr_CsStatus_read;
		Tango::DevLong	*attr_MotorStatus_read;
		Tango::DevLong	*attr_PmacStatus_read;
		Tango::DevDouble	*attr_LCS_read;
		Tango::DevDouble	attr_LCS_write;
		Tango::DevDouble	*attr_TCP_read;
		Tango::DevDouble	attr_TCP_write;
		Tango::DevDouble	*attr_MotorPositions_read;
		Tango::DevUShort	*attr_IsMotorMoving_read;
		Tango::DevUShort	*attr_IsNegativeOn_read;
		Tango::DevUShort	*attr_IsPositiveOn_read;
		Tango::DevUShort	*attr_IsAmplifierEnabled_read;
		Tango::DevUShort	*attr_IsFollowingError_read;
		Tango::DevUShort	*attr_IsHomed_read;
		Tango::DevUShort	*attr_IsActivated_read;
		Tango::DevDouble	*attr_LowLimits_read;
		Tango::DevDouble	attr_LowLimits_write;
		Tango::DevDouble	*attr_HighLimits_read;
		Tango::DevDouble	attr_HighLimits_write;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Hexapod IOC IP
 */
	string	ip;
/**
 *	Hemci port
 */
	Tango::DevShort	port;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	HexapodFMBOExpert(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	HexapodFMBOExpert(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	HexapodFMBOExpert(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~HexapodFMBOExpert() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name HexapodFMBOExpert methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for RsPosition acquisition result.
 */
	virtual void read_RsPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for RxPosition acquisition result.
 */
	virtual void read_RxPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for RzPosition acquisition result.
 */
	virtual void read_RzPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for TsPosition acquisition result.
 */
	virtual void read_TsPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for TxPosition acquisition result.
 */
	virtual void read_TxPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for TzPosition acquisition result.
 */
	virtual void read_TzPosition(Tango::Attribute &attr);
/**
 *	Extract real attribute values for RsSetPosition acquisition result.
 */
	virtual void read_RsSetPosition(Tango::Attribute &attr);
/**
 *	Write RsSetPosition attribute values to hardware.
 */
	virtual void write_RsSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for RxSetPosition acquisition result.
 */
	virtual void read_RxSetPosition(Tango::Attribute &attr);
/**
 *	Write RxSetPosition attribute values to hardware.
 */
	virtual void write_RxSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for RzSetPosition acquisition result.
 */
	virtual void read_RzSetPosition(Tango::Attribute &attr);
/**
 *	Write RzSetPosition attribute values to hardware.
 */
	virtual void write_RzSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for TsSetPosition acquisition result.
 */
	virtual void read_TsSetPosition(Tango::Attribute &attr);
/**
 *	Write TsSetPosition attribute values to hardware.
 */
	virtual void write_TsSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for TxSetPosition acquisition result.
 */
	virtual void read_TxSetPosition(Tango::Attribute &attr);
/**
 *	Write TxSetPosition attribute values to hardware.
 */
	virtual void write_TxSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for TzSetPosition acquisition result.
 */
	virtual void read_TzSetPosition(Tango::Attribute &attr);
/**
 *	Write TzSetPosition attribute values to hardware.
 */
	virtual void write_TzSetPosition(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for Moving acquisition result.
 */
	virtual void read_Moving(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Fault acquisition result.
 */
	virtual void read_Fault(Tango::Attribute &attr);
/**
 *	Extract real attribute values for AllHomed acquisition result.
 */
	virtual void read_AllHomed(Tango::Attribute &attr);
/**
 *	Extract real attribute values for Speed acquisition result.
 */
	virtual void read_Speed(Tango::Attribute &attr);
/**
 *	Write Speed attribute values to hardware.
 */
	virtual void write_Speed(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for CsStatus acquisition result.
 */
	virtual void read_CsStatus(Tango::Attribute &attr);
/**
 *	Extract real attribute values for MotorStatus acquisition result.
 */
	virtual void read_MotorStatus(Tango::Attribute &attr);
/**
 *	Extract real attribute values for PmacStatus acquisition result.
 */
	virtual void read_PmacStatus(Tango::Attribute &attr);
/**
 *	Extract real attribute values for LCS acquisition result.
 */
	virtual void read_LCS(Tango::Attribute &attr);
/**
 *	Write LCS attribute values to hardware.
 */
	virtual void write_LCS(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for TCP acquisition result.
 */
	virtual void read_TCP(Tango::Attribute &attr);
/**
 *	Write TCP attribute values to hardware.
 */
	virtual void write_TCP(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for MotorPositions acquisition result.
 */
	virtual void read_MotorPositions(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsMotorMoving acquisition result.
 */
	virtual void read_IsMotorMoving(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsNegativeOn acquisition result.
 */
	virtual void read_IsNegativeOn(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsPositiveOn acquisition result.
 */
	virtual void read_IsPositiveOn(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsAmplifierEnabled acquisition result.
 */
	virtual void read_IsAmplifierEnabled(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsFollowingError acquisition result.
 */
	virtual void read_IsFollowingError(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsHomed acquisition result.
 */
	virtual void read_IsHomed(Tango::Attribute &attr);
/**
 *	Extract real attribute values for IsActivated acquisition result.
 */
	virtual void read_IsActivated(Tango::Attribute &attr);
/**
 *	Extract real attribute values for LowLimits acquisition result.
 */
	virtual void read_LowLimits(Tango::Attribute &attr);
/**
 *	Write LowLimits attribute values to hardware.
 */
	virtual void write_LowLimits(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for HighLimits acquisition result.
 */
	virtual void read_HighLimits(Tango::Attribute &attr);
/**
 *	Write HighLimits attribute values to hardware.
 */
	virtual void write_HighLimits(Tango::WAttribute &attr);
/**
 *	Read/Write allowed for RsPosition attribute.
 */
	virtual bool is_RsPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for RxPosition attribute.
 */
	virtual bool is_RxPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for RzPosition attribute.
 */
	virtual bool is_RzPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TsPosition attribute.
 */
	virtual bool is_TsPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TxPosition attribute.
 */
	virtual bool is_TxPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TzPosition attribute.
 */
	virtual bool is_TzPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for RsSetPosition attribute.
 */
	virtual bool is_RsSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for RxSetPosition attribute.
 */
	virtual bool is_RxSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for RzSetPosition attribute.
 */
	virtual bool is_RzSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TsSetPosition attribute.
 */
	virtual bool is_TsSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TxSetPosition attribute.
 */
	virtual bool is_TxSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TzSetPosition attribute.
 */
	virtual bool is_TzSetPosition_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Moving attribute.
 */
	virtual bool is_Moving_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Fault attribute.
 */
	virtual bool is_Fault_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for AllHomed attribute.
 */
	virtual bool is_AllHomed_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for Speed attribute.
 */
	virtual bool is_Speed_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for CsStatus attribute.
 */
	virtual bool is_CsStatus_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for MotorStatus attribute.
 */
	virtual bool is_MotorStatus_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for PmacStatus attribute.
 */
	virtual bool is_PmacStatus_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for LCS attribute.
 */
	virtual bool is_LCS_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for TCP attribute.
 */
	virtual bool is_TCP_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for MotorPositions attribute.
 */
	virtual bool is_MotorPositions_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsMotorMoving attribute.
 */
	virtual bool is_IsMotorMoving_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsNegativeOn attribute.
 */
	virtual bool is_IsNegativeOn_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsPositiveOn attribute.
 */
	virtual bool is_IsPositiveOn_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsAmplifierEnabled attribute.
 */
	virtual bool is_IsAmplifierEnabled_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsFollowingError attribute.
 */
	virtual bool is_IsFollowingError_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsHomed attribute.
 */
	virtual bool is_IsHomed_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for IsActivated attribute.
 */
	virtual bool is_IsActivated_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for LowLimits attribute.
 */
	virtual bool is_LowLimits_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for HighLimits attribute.
 */
	virtual bool is_HighLimits_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Start command.
 */
	virtual bool is_Start_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Home command.
 */
	virtual bool is_Home_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Zerouc command.
 */
	virtual bool is_Zerouc_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for DisableAmplifiers command.
 */
	virtual bool is_DisableAmplifiers_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for EnableMotors command.
 */
	virtual bool is_EnableMotors_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Connect command.
 */
	virtual bool is_Connect_allowed(const CORBA::Any &any);
/**
 * Stop all hexapod motion
 *	@exception DevFailed
 */
	void	stop();
/**
 * Start motion. Move to desired set positions.
 *	@exception DevFailed
 */
	void	start();
/**
 * Home motor number arg.
 *	@param	argin	
 *	@exception DevFailed
 */
	void	home(Tango::DevShort);
/**
 * Set LCS as the current UC.
 *	@exception DevFailed
 */
	void	zerouc();
/**
 * Disable all motor amplifiers
 *	@exception DevFailed
 */
	void	disable_amplifiers();
/**
 * Enable all motors.
 *	@exception DevFailed
 */
	void	enable_motors();
/**
 * Connect to hemci.
 *	@exception DevFailed
 */
	void	connect();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------
	
	CslHexapodApi::CslHexapod *hex;
	CslHexapodApi::Config socket;
	
	double low_pos_limit[6];
	double high_pos_limit[6];
	
	double double_val;
	double double_arr[6];
	Tango::DevUShort int_arr[6];
	long status_words[12];
	bool boolean;
};

}	// namespace_ns

#endif	// _HexapodFMBOExpert_H
